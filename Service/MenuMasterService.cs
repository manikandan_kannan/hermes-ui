﻿using Hermes_UI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Hermes_UI.Service
{
    public class MenuMasterService
    {
        public async Task<IEnumerable<MenuViewModel>> GetMenuMaster(string token)
        {
            using var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var result = await client.GetAsync("https://localhost:44311/api/User/GetMenusbyRole");
            var apiResponse = await result.Content.ReadAsAsync<NestedResultSet<MenuViewModel>>(new[] { new JsonMediaTypeFormatter() });
            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return apiResponse.Results;
            }
            else
            {
                return null;
            }            
        }
    }
}
