﻿using Hermes_UI.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Hermes_UI.Controllers
{
    [ValidateSession]    
    public class ManageInvoiceController : Controller
    {
        public IActionResult Index()
        {
            ViewData["authToken"] = HttpContext.Session.GetString("authtoken");
            return View();
        }
        [HttpGet]
        public JsonResult ConvertPDFtoByte(string pdfFilePath)
        {
            try
            {
                byte[] bytes = System.IO.File.ReadAllBytes(pdfFilePath);
                return Json(bytes);
            }
            catch(Exception ex)
            {
                return Json(null);
            }
            
        }
    }
}
