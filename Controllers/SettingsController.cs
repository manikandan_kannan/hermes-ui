﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hermes_UI.Filters;

namespace Hermes_UI.Controllers
{
    public class SettingsController : Controller
    {
        [ValidateSession]
        public IActionResult Index()
        {
            ViewData["authToken"] = HttpContext.Session.GetString("authtoken");
            ViewData["tenant"] = HttpContext.Session.GetString("tenant");
            return View();
        }
    }
}
