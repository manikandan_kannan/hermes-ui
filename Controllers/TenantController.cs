﻿using Hermes_UI.Filters;
using Hermes_UI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Hermes_UI.Controllers
{
    [ValidateSession]
    public class TenantController : Controller
    {
        public IActionResult Index()
        {
            ViewData["authToken"] = HttpContext.Session.GetString("authtoken");
            return View();
        }

    }
}
