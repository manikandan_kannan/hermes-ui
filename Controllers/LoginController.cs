﻿using Hermes_UI.Filters;
using Hermes_UI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;


namespace Hermes_UI.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            ClearSession();
            return View();
        }
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                using var client = new HttpClient();
                var json = JsonConvert.SerializeObject(loginViewModel);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var result = await client.PostAsync("https://localhost:44311/api/user/login", stringContent);
                LoginResponse apiResponse = await result.Content.ReadAsAsync<LoginResponse>(new[] { new JsonMediaTypeFormatter() });
                this.ViewData["MainLayoutViewModel"] = apiResponse;
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (apiResponse != null && apiResponse.IsAuthenticated)
                    {
                        HttpContext.Session.SetString("IsAuthenticated", "true");
                        HttpContext.Session.SetString("authtoken", apiResponse.token);
                        HttpContext.Session.SetString("role", apiResponse.RoleName);
                        HttpContext.Session.SetString("tenant", apiResponse.TenantId);
                        HttpContext.Session.SetString("tenantName", apiResponse.TenantName);
                        HttpContext.Session.SetString("uname", apiResponse.username);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("password", "Invalid Credentials");
                        ClearSession();
                        return View("Index");
                    }

                }
                else
                {
                    return View("Index");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid Credentials");
                return View("Index");
            }

        }

        private void ClearSession()
        {
            HttpContext.Session.SetString("IsAuthenticated", string.Empty);
            HttpContext.Session.SetString("authtoken", string.Empty);
            HttpContext.Session.SetString("role", string.Empty);
            HttpContext.Session.SetString("uname", string.Empty);
            HttpContext.Session.Clear();
            RedirectToAction("Index");
        }
        public async Task<IActionResult> Logout()
        {
            ClearSession();
            return RedirectToAction("Index");
        }
    
}
}
