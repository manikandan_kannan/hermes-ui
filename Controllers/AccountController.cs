﻿using Hermes_UI.EnumConstants;
using Hermes_UI.Filters;
using Hermes_UI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Hermes_UI.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> ConfirmAccount(string Email, string token)
        {
            try
            {
                using var client = new HttpClient();
                var json = JsonConvert.SerializeObject(Email);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var result =  client.GetAsync("https://localhost:44311/api/user/GetConfirmAccountByUser?user=" + Email).Result;
                var apiResponse = await result.Content.ReadAsAsync<NestedResultSet<bool>>(new[] { new JsonMediaTypeFormatter() });
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (apiResponse.IsValid && !apiResponse.Results[0])
                    {
                        var model = new ResetPasswordModel { Token = token, Email = Email };
                        return View(model);
                    }
                    else
                    {
                        TempData["User_confirmed"] = "User_confirmed";
                        return RedirectToAction("Index", "Login");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }


                
                
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "Login");
            }
            
        }
        [HttpPost]
        public async Task<IActionResult> ConfirmAccount(ResetPasswordModel resetPasswordModel)
        {
            if (ModelState.IsValid)
            {
                if(resetPasswordModel.Password != resetPasswordModel.ConfirmPassword)
                {
                    ModelState.AddModelError(string.Empty, "Password and Confirmed Password not Matching.");
                    return View(resetPasswordModel);
                }
                using var client = new HttpClient();
                var json = JsonConvert.SerializeObject(resetPasswordModel);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var result = await client.PostAsync("https://localhost:44311/api/user/ConfirmAccount", stringContent);
                var apiResponse = await result.Content.ReadAsAsync<NestedResultSet<ResetPasswordResponseModel>>(new[] { new JsonMediaTypeFormatter() });
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if(apiResponse.IsValid && apiResponse.Results.Count > 0 && apiResponse.Results[0].isConfirmed)
                    {
                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        foreach(var e in apiResponse.ResultSetErrors)
                        {
                            ModelState.AddModelError("All", e.ErrorMessage);
                        }
                        
                        return View(resetPasswordModel);                        
                    }
                    
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Error occur while processing your request.");
                    return View(resetPasswordModel);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid Credentials/Password and Confirmed Password not Matched");
                return View(resetPasswordModel);
            }
        }
    }
}
