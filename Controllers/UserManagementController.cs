﻿using Hermes_UI.EnumConstants;
using Hermes_UI.Filters;
using Hermes_UI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Hermes_UI.Controllers
{
    [ValidateSession]
    public class UserManagementController : Controller
    {
        public IActionResult Index()
        {
            ViewData["authToken"] = HttpContext.Session.GetString("authtoken");
            ViewData["role"] = GetRoleDrilDown();
            return View();
        }

        private List<RoleDTO> GetRoleDrilDown()
        {
            var result = new List<RoleDTO>();
            if (HttpContext.Session.GetString("role") == "Admin")
            {
                var result1 = (from RoleEnum e in Enum.GetValues(typeof(RoleEnum))
                               select new RoleDTO
                               {
                                   RoleId = (int)e,
                                   RoleName = e.ToString()
                               }).Where(a => a.RoleName != "Admin" && a.RoleName != "SuperAdmin");
                result = result1.ToList();

            }
            else if (HttpContext.Session.GetString("role") == "SuperAdmin")
            {
                var result1 = (from RoleEnum e in Enum.GetValues(typeof(RoleEnum))
                               select new RoleDTO
                               {
                                   RoleId = (int)e,
                                   RoleName = e.ToString()
                               }).Where(a => a.RoleName != "Auditor" && a.RoleName != "Operator" && a.RoleName != "SuperAdmin");
                result = result1.ToList();
            }

            return result;

        }
        
    }
}
