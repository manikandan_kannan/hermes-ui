﻿using Hermes_UI.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Filters
{
    public class ValidateSession : ActionFilterAttribute
    {       
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var value = filterContext.HttpContext.Session.GetString("IsAuthenticated");
            string controller = filterContext.RouteData.Values["controller"].ToString();
            string action = filterContext.RouteData.Values["action"].ToString();
            string token = filterContext.HttpContext.Session.GetString("authtoken");
            var menuObj = new MenuMasterService();
            if(!string.IsNullOrEmpty(token))
            {
                var result = menuObj.GetMenuMaster(token).Result.Where(a=>a.Controller == controller).FirstOrDefault();
                if(result == null)
                {
                    filterContext.HttpContext.Session.SetString("IsAuthenticated", string.Empty);
                    filterContext.HttpContext.Session.SetString("authtoken", string.Empty);
                    filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                    {"controller", "Login"},
                    {"action", "Index"}
                    });
                }
            }
            else
            {
                filterContext.HttpContext.Session.SetString("IsAuthenticated", string.Empty);
                filterContext.HttpContext.Session.SetString("authtoken", string.Empty);
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                    {"controller", "Login"},
                    {"action", "Index"}
                });
            }
            

            if (value != "true")
            {   
                filterContext.HttpContext.Session.SetString("IsAuthenticated",string.Empty);
                filterContext.HttpContext.Session.SetString("authtoken", string.Empty);
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                    {"controller", "Login"},
                    {"action", "Index"}
                });
            }
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            // our code after action executes
        }
    }
}
