﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Models
{
    public class ValidationError
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
