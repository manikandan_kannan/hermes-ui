﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Models
{
    public class LoginResponse
    {

        public string username { get; set; }
        public string token { get; set; }
        public bool IsAuthenticated { get; set; }
        public string TenantName { get; set; }
        public string TenantId { get; set; }
        public string RoleName { get; set; }

    }
}
