﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Models
{
    public class InvoiceUploadViewModel
    {
       
        [Required] 
        public string InvoiceNumber { get; set; }       
        [Required]
        public IFormFile UploadFile { get; set; }
        public string Comments { get; set; }
        public string FileName { get; set; }
    }
}
