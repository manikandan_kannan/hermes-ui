﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Models
{
    public class TenantViewModel
    {
        public string Action { get; set; }
        public string TenantId { get; set; }
        public string TenantName { get; set; }
        public bool IsActive { get; set; }
        public string CreatedOn { get; set; }
    }
}
