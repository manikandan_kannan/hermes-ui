﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Models
{
    public class NestedResultSet<T>
    {
        public List<ValidationError> ResultSetErrors { get; set; }
        public List<T> Results { get; set; }
        public bool IsValid { get; set; }
    }
}
