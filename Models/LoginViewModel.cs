﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Models
{
    public class LoginViewModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
