﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.Models
{
    public class MenuViewModel
    {
        public string MenuName { get; set; }        
        public string Class { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Image { get; set; }
        public string Alt { get; set; }
    }
}
