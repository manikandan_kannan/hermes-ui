﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hermes_UI.EnumConstants
{
    public enum RoleEnum
    {
        Admin = 1,
        Operator = 2,
        Auditor = 3,
        SuperAdmin = 4
    }
    public class RoleDTO
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }

    public class RoleManager
    {
        public IEnumerable<RoleDTO> GetRoles()
        {
            var enumData = from RoleEnum e in Enum.GetValues(typeof(RoleEnum))
                           select new RoleDTO
                           {
                               RoleId = (int)e,
                               RoleName = e.ToString()
                           };

            return enumData;
        }
    }
}
